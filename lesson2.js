
var pets = [
    'kitten',
    'puppy',
    'bird',
    'fish'
];
// start here
// pets is an array of strings

// print the length of the pets array

// print the second item in the array, tip remember, the index begins at 0

// print the last item in the array, try to use the length of the array, rather than 3

// add another pet to the array (your choice), using the push function

// remove the first and last items from the array
// hint: the x.pop() function removes the last item, x.shift() removes the first item

// print out the resulting array