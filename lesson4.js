var number = Math.floor(Math.random() * 10);
// start here

// 'number' is a number in the range [0-9]
// print it below

var number2 = Math.floor(Math.random() * 100);

// 'number2' is a number in the range [0-99]
// print it below

// introducing &&, meaning logical AND
if (number < 5 && number2 <= 50) {
    console.log(number + ' is less than 5 AND ' + number2 + ' is less than 51');
}

// logical OR is represented by ||
// if number is > 5 OR number2 is more than 60 THEN
// print 'number' is more than 5, or 'number2' is more than 60



// end here