// functions allow us to re-use code so we don't need to write it as often

// eg. the function below just prints out 'sing loudly' then prints 'play guitar'
function hello() {
    console.log('sing loudly');
    console.log('play guitar');
}
// you can call it (run it) like this
hello();


// create a function called printTen which prints out numbers from 1-10




// call it below