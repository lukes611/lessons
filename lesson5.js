
var sentence = 'i love cats because cats are so cute';

// sentence is a string
// strings have a function called split, which splits the string into a list
// of strings based on a split character

// eg. sentence.split(' ') with a single space (' ') will split the string by the spaces
// resulting in: [ 'i', 'love', 'cats', 'because', 'cats', 'are', 'so', 'cute' ]

// create a new variable words, which equals the new array created from sentence.split(' ')

// print it below

// the code below will change the words for 'cat', to 'dog'
var wordsListX = sentence.split(' ');
wordsListX[2] = 'puppies';
wordsListX[4] = 'puppies';
// the join function converts from an array back to a string,
// you can pass in a string which connects the items in the array
// uncomment the logs below to see the results
// console.log(wordsListX);
var newSentence = wordsListX.join(' ');
// console.log('new sentence = ' + newSentence);


// can you do something similar with the word 'love'
// change it to 'adore'