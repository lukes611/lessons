
// functions can have return values
// eg
function getInstrument() {
    return 'guitar';
}
function getName() {
    return 'luke';
}

var name = getName();
var instrument = getInstrument();

console.log('my name is ' + name + ' and i like to play ' + instrument);

// create a function called get people
// return an array of 3 people 'luke', 'alex', 'tom'



// call the function and get the returned value
// print the returned value