// functions can have parameters
// see the sum function below
// it has two parameters x and y
function sum(x, y) {
    return x + y;
}

console.log(sum(100, 200));
var fivePlusTen = sum(5, 10);
console.log(fivePlusTen);
// can also be used with strings since + works with strings too
console.log(sum('I want to drink beer ', 'and eat avocado on toast'));

// write a function called 'iLike'
// it must take one variable as a parameter called subject, which will be a string
// make the return value the sentence 'I like [x]' where x is the input of the function



// call the function like this console.log(iLike('bibimbap'));
// and console.log(iLike('bento boxes'));
