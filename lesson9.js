
// see the object below
var x = {
    name: 'luke',
    age: 28
};

console.log(x.name + ' is ' + x.age);

// if age is below 30, print x.name is young


// create yourself as an object and assign to variable 'y'



// print out y


// you can change the name of x, by re-assigning it
x.name = 'peter';
// print out x below to see the new values


// change the 'name' for object y to 'Phuong anh'
// print out the new y object


// fun fact, you can assign new values too
x.name = 'luke'; // <-- change back to luke
x.birthday = '28 FEB 1991';
// print out x to see the new values

// give variable y a birthday too and print it!
